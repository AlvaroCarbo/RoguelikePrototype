﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartItem : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (collision.gameObject.GetComponent<Player>().playerData.health < GameManager.Instance.PlayerMaxHealth)
            {
                Destroy(gameObject);
                collision.gameObject.GetComponent<Player>().playerData.health++;
                GameManager.Instance.Score++;
            }
        }
    }
}
