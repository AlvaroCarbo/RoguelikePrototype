﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "InputData", menuName = "InputData", order = 1)]
public class InputData : ScriptableObject
{
    public Vector2 axisInputDirection;
    public Vector2 mouseScreenPoint;
    public bool spaceInput;
    public bool mouseClickInput;
    public int mouseScrollInput;
}
