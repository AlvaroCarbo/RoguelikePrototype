﻿using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField]
    private InputData inputData;

    public virtual void Update()
    {
        inputData.axisInputDirection = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        inputData.mouseScreenPoint = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
        inputData.spaceInput = Input.GetKey(KeyCode.Space);
        inputData.mouseClickInput = Input.GetKey(KeyCode.Mouse0);
        inputData.mouseScrollInput = (int)Input.mouseScrollDelta.y;
    }
}