﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private TMP_Text bulletText;
    [SerializeField]
    private HealthBar healthBar; 
    [SerializeField]
    private HealthBar reloadBar;
    [SerializeField]
    private HealthBar dashBar;

    [SerializeField]
    private PlayerData playerData;

    private float invincibleTime;
    private bool isInvicible = true;

    private bool isRealoading = false;

    // Start is called before the first frame update
    void Start()
    {
        healthBar = GameObject.Find("HealthBar").GetComponent<HealthBar>();
        reloadBar = GameObject.Find("ReloadBar").GetComponent<HealthBar>();
        dashBar = GameObject.Find("DashBar").GetComponent<HealthBar>();

        healthBar.SetMaxHealth(playerData.health);
        
    }

    // Update is called once per frame

    void FixedUpdate()
    {
        WeaponData weaponData = playerData.playerWeaponsData.PlayerWeapons[playerData.playerWeaponsData.WeaponIndex];
        //bulletText.text = PlayerData.CreateInstance<PlayerData>().ammo.ToString();

        if (weaponData.GetType().Name.Equals("RangeWeaponData"))
        {
            RangeWeaponData rangeWeaponData = (RangeWeaponData)weaponData;

            bulletText.text = $"Player ammo: {playerData.ammo}";

            bulletText.text += $"    {weaponData.name} ammo: {rangeWeaponData.RemainAmmo}";

            if (rangeWeaponData.RemainAmmo <= 0 && !(playerData.ammo == 0))
            {
                if (!isRealoading) {
                    StartCoroutine(UICooldown(rangeWeaponData));
                } else
                {
                    reloadBar.SetHealth(playerData.time - (Time.time - rangeWeaponData.ReloadTime));
                }
                TimeSpan timeSpan = TimeSpan.FromSeconds(playerData.time - (Time.time - rangeWeaponData.ReloadTime));
                bulletText.text = $"Player ammo: {playerData.ammo}      {weaponData.name} reload time: {timeSpan:ss\\.ff}";
            }
        }
        else if (weaponData.GetType().Name.Equals("MeleeWeaponData"))
        {
            bulletText.text = $"Player weapon: {weaponData.name}";
        }
        
        healthBar.SetHealth(playerData.health);


        if (playerData.isInvincible)
        {
            if (isInvicible)
            {
                invincibleTime = Time.time;
                StartCoroutine(UIDashCooldown());
            }
        }
        dashBar.SetHealth(invincibleTime - (Time.time - playerData.dashCDTime));
    }

    private IEnumerator UICooldown(RangeWeaponData rangeWeaponData)
    {
        isRealoading = true;
        reloadBar.SetMaxHealth(playerData.time - (Time.time - rangeWeaponData.ReloadTime));
        yield return new WaitForSeconds(playerData.time - (Time.time - rangeWeaponData.ReloadTime));
        isRealoading = false;
    }
    private IEnumerator UIDashCooldown()
    {
        isInvicible = false;
        dashBar.SetMaxHealth(playerData.dashCDTime);
        yield return new WaitForSeconds(playerData.dashCDTime);
        isInvicible = true;
    }
}
