﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    [SerializeField]
    private GameObject Player;
    [SerializeField]
    private PlayerData playerData;
    
    [SerializeField]
    private GameObject endPanel;

    [SerializeField]
    private Button restartButton;
    
    [SerializeField]
    private TMP_Text scoreText;

    private int playerMaxHealth = 10;
    
    private int score = 0;

    [SerializeField]
    private int totalEnemies;

    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    public int PlayerMaxHealth { get => playerMaxHealth; set => playerMaxHealth = value; }
    public int Score { get => score; set => score = value; }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
    }

    void Start()
    {
        Player = GameObject.Find("Player");
        playerData = Player.GetComponent<Player>().playerData;
        restartButton.onClick.AddListener(RestartGame);
        score = 0;
    }


    // Update is called once per frame
    void Update()
    {
        totalEnemies = GameObject.Find("Enemies").transform.childCount;
        EndGame();
    }

    private void EndGame()
    {
        if (playerData.health <= 0 || totalEnemies <= 0)
        {
            ActivateSceneGO(false);
            endPanel.SetActive(true);
            scoreText.text = $"Score: {Score}";
            Destroy(GameObject.Find("Player"));
            //EndGameUI.SetActive(true);
            //Player.GetComponent<Player>().isDead = true;
        }
    }

    private void RestartGame()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
/*        ActivateSceneGO(true);
        endPanel.SetActive(false);*/
        playerData.health = playerMaxHealth;
    }

    private void ActivateSceneGO(bool flag)
    {
        GameObject.Find("GUI").SetActive(flag);
        GameObject.Find("Grid").SetActive(flag);
        GameObject.Find("Enemies").SetActive(flag);
        GameObject.Find("DestructableObjects").SetActive(flag);
    }
}

