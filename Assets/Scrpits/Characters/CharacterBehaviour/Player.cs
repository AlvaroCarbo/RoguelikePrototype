﻿using System;
using System.Collections;
using UnityEngine;

public class Player : Character
{
    [SerializeField]
    private InputData inputData;

    [SerializeField]
    public PlayerData playerData;

    [SerializeField]
    private GameObject bullet;

    [SerializeField]
    private WeaponData weapon;


    private void Awake()
    {
        //playerWeaponsCount = playerWeapons.PlayerWeapons.Count();
    }

    public override void Start()
    {
        base.Start();

        playerData.isHitted = false;
        playerData.canDash = true;


        if (playerData.playerWeaponsData.PlayerWeapons[playerData.playerWeaponsData.WeaponIndex].GetType().Name.Equals("RangeWeaponData"))
        {
            RangeWeaponData weapon = (RangeWeaponData)playerData.playerWeaponsData.PlayerWeapons[playerData.playerWeaponsData.WeaponIndex];
            weapon.canShoot = true;
            playerData.playerWeaponsData.PlayerWeapons[playerData.playerWeaponsData.WeaponIndex] = weapon;
        } else if  (playerData.playerWeaponsData.PlayerWeapons[playerData.playerWeaponsData.WeaponIndex].GetType().Name.Equals("MeleeWeaponData"))
        {
            playerData.canAttack = true;
        }

        SetWeapon();

    }

    private void Update()
    {
        playerData.moveDirection = Vector2.ClampMagnitude(inputData.axisInputDirection, 1);

        ChangeWeapon();
    }

    private void FixedUpdate()
    {
        /*        float angle = Vector3.Angle(transform.position, inputData.mouseScreenPoint - new Vector2(transform.position.x, transform.position.y));
                Debug.Log(angle);*/
        if (inputData.mouseScreenPoint.x > transform.position.x)
        {
            transform.GetChild(0).position = new Vector3(transform.position.x + 0.5f, transform.position.y - 0.3f, transform.GetChild(0).position.z);
            GetComponent<SpriteRenderer>().flipX = false;
            transform.GetChild(0).GetComponent<SpriteRenderer>().flipY = false;
        }
        else
        {
            transform.GetChild(0).position = new Vector3(transform.position.x - 0.5f, transform.position.y - 0.3f, transform.GetChild(0).position.z);
            GetComponent<SpriteRenderer>().flipX = true;
            transform.GetChild(0).GetComponent<SpriteRenderer>().flipY = true;

        }

        if (inputData.spaceInput) Dash();
        Move();

        playerData.mouseDirection = (inputData.mouseScreenPoint - GetComponent<Rigidbody2D>().position).normalized;

        //Rotate Weapon on mouse dir
        transform.GetChild(0).transform.rotation = Quaternion.Euler(0f, 0f, Mathf.Atan2(GetComponentInParent<Player>().playerData.mouseDirection.y, GetComponentInParent<Player>().playerData.mouseDirection.x) * Mathf.Rad2Deg);

        //Rotate player on mouse dir
        //GetComponent<Rigidbody2D>().rotation = Mathf.Atan2(playerData.mouseDirection.y, playerData.mouseDirection.x) * Mathf.Rad2Deg + 90;

        //Rotate player on input axis dir
        //Rb.rotation = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg + 90;


        /*Debug.DrawRay(transform.GetChild(0).position, new Vector2(playerData.mouseDirection.y, -playerData.mouseDirection.x), Color.blue);
        Debug.DrawRay(transform.GetChild(0).position, new Vector2(-playerData.mouseDirection.y, playerData.mouseDirection.x), Color.yellow);*/

        if (inputData.mouseClickInput) Attack();



    }

    private void ChangeWeapon()
    {
        int weaponIndex = playerData.playerWeaponsData.WeaponIndex;
        int maxIndex = playerData.playerWeaponsData.PlayerWeapons.Count;
        weaponIndex += inputData.mouseScrollInput;
        if (weaponIndex != playerData.playerWeaponsData.WeaponIndex)
        {
            weaponIndex = (weaponIndex < 0) ? maxIndex + weaponIndex : (weaponIndex >= maxIndex) ? 0 : weaponIndex;
            playerData.playerWeaponsData.WeaponIndex = weaponIndex;
            SetWeapon();
        }
    }

    private void SetWeapon()
    {
        weapon = playerData.playerWeaponsData.PlayerWeapons[playerData.playerWeaponsData.WeaponIndex];
        transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = weapon.weaponSprite;
    }

    public override void Move()
    {
        if (inputData.axisInputDirection.x != 0 || inputData.axisInputDirection.y != 0)
        {
            GetComponent<Animator>().SetBool("isMoving", true);
            GetComponent<Rigidbody2D>().AddForce(playerData.moveDirection * playerData.velocity, ForceMode2D.Force);
        }
        else
        {
            GetComponent<Animator>().SetBool("isMoving", false);
        }
    }

    private void Dash()
    {
        if (playerData.canDash)
        {
            StartCoroutine(InvincibleTime());
            GetComponent<Rigidbody2D>().AddForce(playerData.moveDirection * playerData.dashForce, ForceMode2D.Impulse);
            StartCoroutine(DashCooldown());
        }
    }

    private IEnumerator InvincibleTime()
    {
        playerData.isInvincible = true;
        transform.GetChild(2).gameObject.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        transform.GetChild(2).gameObject.SetActive(false);
        playerData.isInvincible = false;
    }

    public override void Attack()
    {
        if (playerData.playerWeaponsData.PlayerWeapons[playerData.playerWeaponsData.WeaponIndex].GetType().Name.Equals("RangeWeaponData"))
        {
            RangeWeaponData weapon = (RangeWeaponData)playerData.playerWeaponsData.PlayerWeapons[playerData.playerWeaponsData.WeaponIndex];
            if (playerData.ammo > 0 || weapon.RemainAmmo > 0)
            {
                if (weapon.canShoot)
                    PullTrigger(weapon);
            }

        }
        else
        {
            if (playerData.canAttack)
                StartCoroutine(MeleeAttack());
            //Do melee attack
        }
    }

    public void PullTrigger(RangeWeaponData weapon)
    {
        int bulletsToShoot = (weapon.BulletsXShoot <= weapon.RemainAmmo) ? weapon.BulletsXShoot : (weapon.BulletsXShoot - weapon.RemainAmmo);
        for (int i = 0; i < bulletsToShoot; i++)
        {
            float angle = weapon.angleFromOtherBullet;
            //int temp;
            //temp = (i % 2 != 0) ? i : -i;
            angle = (i % 2 != 0) ? angle : -angle;
/*            Debug.Log(temp + " " + angle);*/

            //Debug.Log(bulletsToShoot % 2 == 0);
            if (bulletsToShoot % 2 != 0)
            {
                angle = (i == 0) ? 0 : angle;
                angle = (i > 2) ? angle * 2 : angle;
                angle = (i > 4) ? angle * 3 : angle;
                Shoot(weapon, angle);
            }
            else
            {
                switch (i)
                {
                    case 0:
                        Shoot(weapon, 2.5f);
                        break;
                    case 1:
                        Shoot(weapon, -2.5f);
                        break;
                    case 2:
                        Shoot(weapon, 7.5f);
                        break;
                    case 3:
                        Shoot(weapon, -7.5f);
                        break;
                }
            }
            weapon.RemainAmmo -= 1;

        }
        if (weapon.RemainAmmo > 0)
        {
            StartCoroutine(ShootCooldown(weapon));
        }
        else
        {
            StartCoroutine(Reloading(weapon));
        }
        GetComponent<Rigidbody2D>().AddForce(-playerData.mouseDirection * weapon.SetbackMomentum, ForceMode2D.Impulse);
    }

    private void Shoot(RangeWeaponData weapon, float angle)
    {
        Vector2 direction = Quaternion.AngleAxis(angle, Vector3.forward) * playerData.mouseDirection;
        GameObject shoot = Instantiate(bullet, transform.GetChild(0).position, Quaternion.identity);
        BulletData bulletData = weapon.bulletData;
        shoot.GetComponent<Rigidbody2D>().AddForce(direction * bulletData.Force, ForceMode2D.Impulse);
        shoot.GetComponent<SpriteRenderer>().sprite = bulletData.bulletSprite;
        shoot.transform.up = direction;
        //Debug.DrawRay(transform.GetChild(0).position, direction, Color.red);
    }

    private IEnumerator MeleeAttack()
    {
        playerData.canAttack = false;
        GetComponent<SpriteRenderer>().color = Color.red;
        yield return new WaitForSeconds(1);
        playerData.canAttack = true;
        GetComponent<SpriteRenderer>().color = Color.white;

    }
    private IEnumerator DashCooldown()
    {
        playerData.canDash = false;
        yield return new WaitForSeconds(playerData.dashCDTime);
        playerData.canDash = true;
    }

    private IEnumerator Reloading(RangeWeaponData weapon)
    {
        weapon.canShoot = false;
        playerData.time = Time.time;
        yield return new WaitForSeconds(weapon.ReloadTime);
        weapon.RemainAmmo = (playerData.ammo > weapon.MaxAmmo) ? weapon.MaxAmmo : playerData.ammo;
        playerData.ammo -= (playerData.ammo > 0) ? weapon.RemainAmmo : 0;
        weapon.canShoot = true;
    }

    private IEnumerator ShootCooldown(RangeWeaponData weapon)
    {
        weapon.canShoot = false;
        yield return new WaitForSeconds(weapon.FireRate);
        weapon.canShoot = true;
    }

    private IEnumerator HurtAnimation()
    {
        GetComponent<SpriteRenderer>().color = Color.red;
        playerData.isHitted = true;
        GetComponent<Animator>().SetBool("isHitted", true);
        yield return new WaitForSeconds(0.1f);
        GetComponent<SpriteRenderer>().color = Color.white;
        GetComponent<Animator>().SetBool("isHitted", false);
        yield return new WaitForSeconds(0.4f);
        playerData.isHitted = false;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            if (!playerData.isInvincible)
            {
                if (!playerData.isHitted)
                {
                    playerData.health -= collision.gameObject.transform.parent.GetComponent<Enemy>().enemyData.damage;
                    StartCoroutine(HurtAnimation());

                    if (collision.GetComponent<Rigidbody2D>() != null) // knockback
                    {
                        Vector2 knockbackDirection = collision.GetComponent<Rigidbody2D>().position - GetComponent<Rigidbody2D>().position;
                        collision.GetComponent<Rigidbody2D>().AddForce(knockbackDirection * 20, ForceMode2D.Impulse);
                    }
                }
            }
            else
            {
            // Kill Enemies when invicible
                Destroy(collision.gameObject.transform.parent.gameObject);
                GameManager.Instance.Score += 2;
            }
        }

        if (collision.CompareTag("EnemyBullet"))
        {
            if (!playerData.isInvincible)
            {
                if (!playerData.isHitted)
                {
                    playerData.health--;
                    StartCoroutine(HurtAnimation());
                    if (collision.GetComponent<Rigidbody2D>() != null) // knockback
                    {
                        Vector2 knockbackDirection = collision.GetComponent<Rigidbody2D>().position - GetComponent<Rigidbody2D>().position;
                        collision.GetComponent<Rigidbody2D>().AddForce(knockbackDirection * 20, ForceMode2D.Impulse);
                    }
                    Destroy(collision.gameObject);
                }
            }
            else Destroy(collision.gameObject);
        }

        if (playerData.isInvincible && collision.CompareTag("Destructable"))
        {
            // Destroy Objects when invicible
            collision.gameObject.GetComponent<DestructableObjects>().DropItem();
            Destroy(collision.gameObject);
            GameManager.Instance.Score++;
        }
    }
}
