﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{



    public virtual void Start()
    {

    }

    void Update()
    {
        
    }

    public virtual void Move()
    {

    }    
    
    public virtual void Attack()
    {

    }
}
