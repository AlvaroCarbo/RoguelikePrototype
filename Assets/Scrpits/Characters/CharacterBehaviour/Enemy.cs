﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{
    [SerializeField]
    public EnemyData enemyData;
    [SerializeField]
    private bool canExplote;
    [SerializeField]
    private bool canMove;
    [SerializeField]
    private bool canShoot;
    [SerializeField]
    private GameObject enemyBullet;

    public override void Start()
    {

    }


    private void FixedUpdate()
    {
        if (canMove) 
            Move();
        if (canShoot)
            Shoot();
    }

    private void Shoot()
    {
        Vector2 direction = GameObject.Find("Player").transform.position - transform.position;
        GameObject shoot = Instantiate(enemyBullet, transform.position, Quaternion.identity);
        shoot.GetComponent<Rigidbody2D>().AddForce(direction.normalized * 10f, ForceMode2D.Impulse);
        shoot.transform.up = direction;
        StartCoroutine(ShootCooldown());
    }

    private IEnumerator ShootCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(2f);
        canShoot = true;
    }

    public override void Move()
    {
        // Move our position a step closer to the target.
        float step = enemyData.velocity * Time.deltaTime; // calculate distance to move
        if (GameObject.Find("Player"))
        {
            transform.position = Vector3.MoveTowards(gameObject.transform.position, GameObject.Find("Player").transform.position, step);

            GetComponent<SpriteRenderer>().flipX = (transform.position.x > GameObject.Find("Player").transform.position.x) ? true : false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet"))
        {
            Destroy(gameObject);
            Destroy(collision.gameObject);
            GameManager.Instance.Score += 5;

        }
        else if (collision.CompareTag("Player"))
        {
            if (collision.GetComponent<Rigidbody2D>() != null)
            {
                Vector2 direction = collision.GetComponent<Rigidbody2D>().position - GetComponent<Rigidbody2D>().position;
                collision.GetComponent<Rigidbody2D>().AddForce(direction * 10, ForceMode2D.Impulse);
                if (canExplote)
                {
                    canMove = false;
                    GetComponent<Animator>().SetBool("Explosion", true);
                    StartCoroutine(DestroyItself());
                }
            }
        }
    }

    private IEnumerator DestroyItself()
    {
        gameObject.GetComponent<Collider2D>().enabled = false;
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }
}
