﻿using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Wall") || collision.CompareTag("Destructable"))
        {
            Destroy(gameObject);
        }

        if (gameObject.CompareTag("EnemyBullet") && collision.CompareTag("Player"))
        {
            //Destroy(gameObject);
        }
    }
}