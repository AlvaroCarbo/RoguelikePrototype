﻿using UnityEngine;

public abstract class WeaponData : ScriptableObject
{
    public Sprite weaponSprite;
}