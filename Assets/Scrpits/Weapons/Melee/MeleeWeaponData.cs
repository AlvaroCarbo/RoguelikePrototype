﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MeleeWeaponData", menuName = "Weapons/MeleeWeaponData", order = 1)]
public class MeleeWeaponData : WeaponData
{
    public int Damge;
}
