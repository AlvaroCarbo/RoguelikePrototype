﻿using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField]
    private Vector2 attackDirection;

    [SerializeField]
    private Player player;

    [SerializeField]
    private float attackCDTime;

    public Vector2 AttackDirection { get => attackDirection; set => attackDirection = value; }
    public float AttackCDTime { get => attackCDTime; set => attackCDTime = value; }

    // Start is called before the first frame update
    private void Start()
    {
    }

    // Update is called once per frame
    public virtual void Update()
    {
        player = GetComponent<Player>();
        /*        attackDirection = player.MouseDirection;        */
    }

    /*    IEnumerator AttackColdown()
        {
                player.canShoot = false;
                yield return new WaitForSeconds(shootCDTime);
                canShoot = true;
        }*/
}