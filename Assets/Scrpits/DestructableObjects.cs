﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructableObjects : MonoBehaviour
{
    [SerializeField]
    private GameObject[] items;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet") || collision.CompareTag("EnemyBullet"))
        {
            Destroy(gameObject);
            DropItem();
            if (collision.CompareTag("Bullet"))
                GameManager.Instance.Score++;
        }
    }

    public void DropItem()
    {
        Debug.Log(transform.position);    
        Instantiate(items[0], transform.position, Quaternion.identity);
    }
}
